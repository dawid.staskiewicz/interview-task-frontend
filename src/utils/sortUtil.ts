import { SortType } from "@/types/SortType";
import { Stop } from "@/types/Stop";

export function sortUtil(a: Stop, b: Stop, sortType: SortType): 1 | -1 {
	if (sortType === "desc") {
		return (a.order > b.order) ? -1 : 1
	}
	return (a.order > b.order) ? 1 : -1
}
