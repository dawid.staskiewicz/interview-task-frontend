import { createStore } from 'vuex'
import { Api } from "@/api";
import { sortUtil } from "@/utils/sortUtil";
import { SelectedStop } from "@/types/SelectedStop";
import { SelectedLine } from "@/types/SelectedLine";
import { Stop } from "@/types/Stop";
import { SortType } from "@/types/SortType";

export default createStore({
  state: {
    isLoading: false,
    stops: [] as Stop[],
    error: "",
    selectedLine: {
      line: null,
      stops: [],
      uniqueStops: [],
    } as SelectedLine,
    selectedStop: {
      stop: null,
      times: [],
    } as SelectedStop,
  },
  getters: {
    getUniqueLines(state) {
      const uniqueLines = new Set([...state.stops.map((item: Stop) => item.line)])

      return [...uniqueLines].sort()
    },
  },
  mutations: {
    setLoading(state, isLoading: boolean) {
      state.isLoading = isLoading
    },
    setStops(state, payload: Stop[]) {
      state.stops = payload
    },
    setError(state, error: string) {
      state.error = error
    },
    selectLine(state, payload: number) {
      const stops = state.stops.filter((item: Stop) => item.line === payload)
      const uniqueStopsNames = [...new Set(stops.map((item: Stop) => item.stop))]
      const uniqueStops: Stop[] = []
      uniqueStopsNames.forEach((name: string) => {
        const stop: Stop | undefined = stops.find((item: Stop) => item.stop === name)
        if (stop) {
          uniqueStops.push(stop)
        }
      })

      state.selectedLine = {
        line: payload,
        stops: [...stops],
        uniqueStops: [...uniqueStops],
      }

      state.selectedStop = {
        stop: null,
        times: [],
      }
    },
    selectStop(state, payload: string) {
      const stops: Stop[] = state.selectedLine.stops.filter(
        (item: Stop): boolean => item.stop === payload
      )
      state.selectedStop = {
        stop: payload,
        times: stops
          .map((item: Stop) => item.time)
          .sort((a: string, b: string): number => {
            const first: string = a.replace(':', '')
            const second: string = b.replace(':', '')

            return Number(first) > Number(second) ? 1 : -1
          }),
      }
    },
    sortBusStops(state, payload: SortType) {
      state.selectedLine = {
        line: state.selectedLine.line,
        stops: state.selectedLine.stops,
        uniqueStops: state.selectedLine.uniqueStops.sort((a: Stop, b: Stop) =>
          sortUtil(a, b, payload)
        ),
      };
    },
    sortAllBusStops(state, payload: SortType) {
      state.stops = state.stops.sort((a: Stop, b: Stop) =>
        sortUtil(a, b, payload)
      );
    },

  },
  actions: {
    async fetchStops({ commit }) {
      try {
        commit("setLoading", true)
        const { data } = await Api.get("/stops")
        commit("setStops", data)
      } catch (error) {
        console.error("error", error)
        if (error instanceof Error) {
          commit("setError", error.message || error)
        }
      } finally {
        commit("setLoading", false)
      }
    },
  },
})
