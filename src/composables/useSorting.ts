import { ref, computed } from "vue";
import { SortType } from "@/types/SortType";

const DEFAULT_SORT_TYPE: SortType = "asc";
export default function useSorting() {
	const sortType = ref<SortType>(DEFAULT_SORT_TYPE);

	const sort = (cb: (t: SortType) => void): void => {
		sortType.value = sortType.value === "asc" ? "desc" : "asc";

		cb(sortType.value);
	}

	return {
		sortType: computed(() => sortType.value),
		sort,
	};
}
