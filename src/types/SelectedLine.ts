import { Stop } from "@/types/Stop";

export type SelectedLine = {
	line: number | null;
	stops: Stop[];
	uniqueStops: Stop[];
};
