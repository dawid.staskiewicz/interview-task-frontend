export type SelectedStop = {
	stop: string | null;
	times: string[];
}
